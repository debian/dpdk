Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: dpdk
Source: https://dpdk.org

Files: *
Copyright:
           1999-2024 Intel Corporation.
           2006-2019 Solarflare Communications Inc.
           2007      Nuova Systems, Inc.
           2007      VMware, Inc.
           2008-2024 Cisco Systems, Inc.
           2012-2021 6WIND S.A.
           2012-2022 Mellanox.
           2014-2023 Broadcom
           2014-2020 Chelsio Communications
           2014-2024 IBM Corporation.
           2014-2021 Netronome Systems, Inc.
           2015-2020 Amazon.com, Inc. or its affiliates
           2015-2020 Cavium, Inc
           2015-2024 Beijing WangXun Technology Co., Ltd.
           2016-2025 Linaro Limited
           2016-2023 Microsoft Corporation
           2017-2024 Marvell International Ltd.
           2017-2023 Red Hat, Inc.
           2019-2022 Xilinx, Inc.
           2018-2024 HiSilicon Limited.
           2018-2024 Ericsson AB
           2018-2024 Arm Limited
           2018-2024 Advanced Micro Devices, Inc.
           2021-2024 NVIDIA Corporation & Affiliates
           2024-2025 Huawei Technologies Co.,Ltd.
           2024 Corigine, Inc.
           2024 Napatech A/S
           2024 PANTHEON.tech s.r.o.
           2024 University of New Hampshire
           2024 The DPDK contributors
           and many other contributors.
License: BSD-3-clause

Files:
 drivers/crypto/qat/qat_sym_session.c
 drivers/crypto/dpaa2_sec/mc/dpseci.c
 drivers/crypto/dpaa2_sec/mc/fsl_dpseci_cmd.h
 drivers/crypto/dpaa2_sec/mc/fsl_dpseci.h
 drivers/common/qat/qat_adf/adf_transport_access_macros_gen4.h
 drivers/common/qat/qat_adf/icp_qat_hw.h
 drivers/common/qat/qat_adf/icp_qat_fw_la.h
 drivers/common/qat/qat_adf/adf_transport_access_macros.h
 drivers/common/qat/qat_adf/adf_transport_access_macros_gen4vf.h
 drivers/common/qat/qat_adf/icp_qat_fw_comp.h
 drivers/common/qat/qat_adf/adf_pf2vf_msg.h
 drivers/common/qat/qat_adf/icp_qat_fw.h
 drivers/common/qat/qat_adf/icp_qat_hw_gen4_comp_defs.h
 drivers/common/qat/qat_adf/icp_qat_hw_gen4_comp.h
 drivers/common/dpaax/compat.h
 drivers/common/dpaax/caamflib.c
 drivers/common/dpaax/dpaa_of.c
 drivers/common/dpaax/dpaa_of.h
 drivers/common/dpaax/caamflib/rta/store_cmd.h
 drivers/common/dpaax/caamflib/rta/load_cmd.h
 drivers/common/dpaax/caamflib/rta/signature_cmd.h
 drivers/common/dpaax/caamflib/rta/fifo_load_store_cmd.h
 drivers/common/dpaax/caamflib/rta/move_cmd.h
 drivers/common/dpaax/caamflib/rta/key_cmd.h
 drivers/common/dpaax/caamflib/rta/seq_in_out_ptr_cmd.h
 drivers/common/dpaax/caamflib/rta/protocol_cmd.h
 drivers/common/dpaax/caamflib/rta/sec_run_time_asm.h
 drivers/common/dpaax/caamflib/rta/nfifo_cmd.h
 drivers/common/dpaax/caamflib/rta/jump_cmd.h
 drivers/common/dpaax/caamflib/rta/header_cmd.h
 drivers/common/dpaax/caamflib/rta/math_cmd.h
 drivers/common/dpaax/caamflib/rta/operation_cmd.h
 drivers/common/dpaax/caamflib/compat.h
 drivers/common/dpaax/caamflib/desc.h
 drivers/common/dpaax/caamflib/rta.h
 drivers/common/dpaax/caamflib/desc/algo.h
 drivers/common/dpaax/caamflib/desc/common.h
 drivers/common/dpaax/caamflib/desc/ipsec.h
 drivers/net/dpaa/fmlib/ncsw_ext.h
 drivers/net/dpaa/fmlib/dpaa_integration.h
 drivers/net/dpaa/fmlib/net_ext.h
 drivers/net/dpaa2/mc/dprtc.c
 drivers/net/dpaa2/mc/fsl_dprtc.h
 drivers/net/dpaa2/mc/fsl_dpdmux.h
 drivers/net/dpaa2/mc/fsl_dpni.h
 drivers/net/dpaa2/mc/fsl_net.h
 drivers/net/dpaa2/mc/fsl_dpkg.h
 drivers/net/dpaa2/mc/dpkg.c
 drivers/net/dpaa2/mc/fsl_dprtc_cmd.h
 drivers/net/dpaa2/mc/dpni.c
 drivers/net/dpaa2/mc/fsl_dpni_cmd.h
 drivers/net/dpaa2/mc/fsl_dpdmux_cmd.h
 drivers/net/dpaa2/mc/dpdmux.c
 drivers/net/atlantic/atl_hw_regs.c
 drivers/net/atlantic/atl_hw_regs.h
 drivers/net/atlantic/hw_atl/hw_atl_utils.c
 drivers/net/atlantic/hw_atl/hw_atl_b0.c
 drivers/net/atlantic/hw_atl/hw_atl_b0_internal.h
 drivers/net/atlantic/hw_atl/hw_atl_llh.h
 drivers/net/atlantic/hw_atl/hw_atl_utils.h
 drivers/net/atlantic/hw_atl/hw_atl_utils_fw2x.c
 drivers/net/atlantic/hw_atl/hw_atl_b0.h
 drivers/net/atlantic/hw_atl/hw_atl_llh_internal.h
 drivers/net/atlantic/hw_atl/hw_atl_llh.c
 drivers/net/bnxt/bnxt_nvm_defs.h
 drivers/bus/dpaa/include/fsl_fman_crc64.h
 drivers/bus/dpaa/include/fman.h
 drivers/bus/dpaa/include/process.h
 drivers/bus/dpaa/include/fsl_fman.h
 drivers/bus/dpaa/include/fsl_bman.h
 drivers/bus/dpaa/include/netcfg.h
 drivers/bus/dpaa/include/fsl_usd.h
 drivers/bus/dpaa/include/fsl_qman.h
 drivers/bus/dpaa/base/qbman/qman_driver.c
 drivers/bus/dpaa/base/qbman/process.c
 drivers/bus/dpaa/base/qbman/dpaa_sys.c
 drivers/bus/dpaa/base/qbman/bman_driver.c
 drivers/bus/dpaa/base/qbman/dpaa_alloc.c
 drivers/bus/dpaa/base/qbman/qman.c
 drivers/bus/dpaa/base/qbman/bman.h
 drivers/bus/dpaa/base/qbman/bman.c
 drivers/bus/dpaa/base/qbman/qman_priv.h
 drivers/bus/dpaa/base/qbman/bman_priv.h
 drivers/bus/dpaa/base/qbman/qman.h
 drivers/bus/dpaa/base/qbman/dpaa_sys.h
 drivers/bus/dpaa/base/fman/fman.c
 drivers/bus/dpaa/base/fman/netcfg_layer.c
 drivers/bus/fslmc/mc/fsl_dpci.h
 drivers/bus/fslmc/mc/dpci.c
 drivers/bus/fslmc/mc/fsl_dprc.h
 drivers/bus/fslmc/mc/fsl_dpmng_cmd.h
 drivers/bus/fslmc/mc/dpbp.c
 drivers/bus/fslmc/mc/dpcon.c
 drivers/bus/fslmc/mc/fsl_mc_cmd.h
 drivers/bus/fslmc/mc/fsl_dpcon_cmd.h
 drivers/bus/fslmc/mc/fsl_dpbp_cmd.h
 drivers/bus/fslmc/mc/fsl_dpcon.h
 drivers/bus/fslmc/mc/fsl_mc_sys.h
 drivers/bus/fslmc/mc/fsl_dpopr.h
 drivers/bus/fslmc/mc/fsl_dpbp.h
 drivers/bus/fslmc/mc/fsl_dprc_cmd.h
 drivers/bus/fslmc/mc/mc_sys.c
 drivers/bus/fslmc/mc/dpmng.c
 drivers/bus/fslmc/mc/dprc.c
 drivers/bus/fslmc/mc/fsl_dpci_cmd.h
 drivers/bus/fslmc/mc/fsl_dpio.h
 drivers/bus/fslmc/mc/fsl_dpio_cmd.h
 drivers/bus/fslmc/mc/fsl_dpmng.h
 drivers/bus/fslmc/mc/dpio.c
 lib/eal/include/rte_pci_dev_features.h
 lib/eal/include/rte_pci_dev_feature_defs.h
Copyright: 2008-2016 Freescale Semiconductor Inc.
           2011 Freescale Semiconductor, Inc.
           2014-2017 aQuantia Corporation.
           2014-2023 Broadcom
           2016-2024 NXP
           2010-2022 Intel Corporation
License: BSD-3-clause or GPL-2

Files:
 drivers/net/avp/rte_avp_common.h
 drivers/net/avp/rte_avp_fifo.h
Copyright: 2010-2013 Intel Corporation.
           2013-2017 Wind River Systems, Inc.
License: BSD-3-clause or LGPL-2.1

Files: debian/*
Copyright: 2015-2024 Canonical Ltd.
           2016-2024 Luca Boccassi <bluca@debian.org>
License: GPL-3

License: GPL-2
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: LGPL-2.1
 On Debian systems, the complete text of the GNU Library General Public
 License can be found in the file `/usr/share/common-licenses/LGPL-2.1'.

Files:
 lib/eal/windows/include/dirent.h
Copyright: 2006-2012 Toni Ronkko
License: expat

Files:
 drivers/net/gve/base/*
Copyright: 2015-2022 Google, Inc.
License: expat

Files:
 lib/eal/windows/include/getopt.h
Copyright: 2000 The NetBSD Foundation, Inc.
License: BSD-2-clause

Files:
 lib/eal/windows/getopt.c
Copyright: 2002 Todd C. Miller <Todd.Miller@courtesan.com>
License: ISC and BSD-2-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in
   the documentation and/or other materials provided with the
   distribution.
 * Neither the name of Intel Corporation nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE

License: BSD-2-clause
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are
  met:
  .
     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.
     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in
        the documentation and/or other materials provided with the
        distribution.
  .
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: ISC
  Permission to use, copy, modify, and distribute this software for any
  purpose with or without fee is hereby granted, provided that the above
  copyright notice and this permission notice appear in all copies.
  .
  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
