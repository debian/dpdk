#!/bin/sh
set -uxe

# since these tests really execute dpdk code they have to check for the
# required minimum cpu features
ARCH=$(dpkg --print-architecture)
echo "Check required features on arch: ${ARCH}"
case "${ARCH}" in
    amd64)
        if ! grep -q '^flags.*sse4_2' /proc/cpuinfo; then
            echo "Missing sse4_2 on ${ARCH} - not supported, SKIP test"
            exit 77
        fi
        ;;
    arm64)
        if ! grep -q '^Features.*crc32' /proc/cpuinfo; then
            echo "Missing crc32 on ${ARCH} - not supported, SKIP test"
            exit 77
        fi
        ;;
esac
echo "no known missing feature on ${ARCH}, continue test"

meson setup build -Dplatform=generic
# XDG_RUNTIME_DIR will be used for an AF unix socket, so override it to avoid
# permission errors and leaving files around
SOCKET_DIR=$(mktemp --directory)
ret=0
# Default is 10, 3*10 seemed to be not enough (was flaky)
# Pick 12*10 which right now (22.11) would be max ~<=100 tests => ~3h max
MESON_TESTTHREADS=1 XDG_RUNTIME_DIR=${SOCKET_DIR} meson test -C build --verbose --suite fast-tests -t 12 || ret=$?
if [ $ret -eq 125 ]; then
    echo "Warning: failed in build stage before actual tests skipping"
    exit 77
fi
rm -rf "${SOCKET_DIR}"
exit $ret
